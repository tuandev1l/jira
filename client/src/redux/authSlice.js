import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import Cookies from 'universal-cookie';
import { axiosPro } from './CONSTANTS.js';

export const loginThunk = createAsyncThunk(
  'auth/login',
  async (user, { rejectWithValue }) => {
    try {
      const { data } = await axiosPro.post('login', user);
      console.log({ data });
      return data;
    } catch (error) {
      console.log({ error });
      return rejectWithValue(error);
    }
  }
);

export const signupThunk = createAsyncThunk(
  'auth/signup',
  async (user, { rejectWithValue }) => {
    try {
      const { data } = await axiosPro.post('signup', user);
      console.log(data);
      return data;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

const authSlice = createSlice({
  name: 'auth',
  initialState: {
    isLoading: false,
    isLogin: !!localStorage.getItem('token'),
    err: false,
  },
  reducers: {
    logout: (state, action) => {
      state.isLogin = false;
      localStorage.clear();
    },
  },
  extraReducers: {
    [loginThunk.pending]: (state, action) => {
      state.isLoading = true;
    },
    [loginThunk.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.isLogin = true;
      localStorage.setItem('name', action.payload.data.user.name);
      localStorage.setItem('email', action.payload.data.user.email);
      localStorage.setItem('avatar', action.payload.data.user.avatar);
      localStorage.setItem('token', action.payload.token);
    },
    [loginThunk.rejected]: (state, action) => {
      state.isLoading = false;
      state.isLogin = false;
      state.err = true;
    },
    [signupThunk.pending]: (state, action) => {
      state.isLoading = true;
    },
    [signupThunk.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.isLogin = true;
      localStorage.setItem('name', action.payload.data.user.name);
      localStorage.setItem('email', action.payload.data.user.email);
      localStorage.setItem('avatar', action.payload.data.user.avatar);
      localStorage.setItem('token', action.payload.token);
    },
    [signupThunk.rejected]: (state, action) => {
      state.isLoading = false;
      state.isLogin = false;
      state.err = true;
    },
  },
});

export default authSlice;

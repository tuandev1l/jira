import { Box } from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Container, Draggable } from 'react-smooth-dnd';
import todoSlice, {
  updateStatusTaskThunk,
  updateTodoThunk,
} from '../../redux/todoSlice.js';
import Todo from './Todo.js';

const TodoItems = ({ column, todos }) => {
  const [data, setData] = useState([]);
  const dispatch = useDispatch();

  useEffect(() => {
    setData(todos.filter((todo) => todo.status === column.status));
  }, [todos]);

  const applyDrag = (arr, dragResult) => {
    const { removedIndex, addedIndex, payload } = dragResult;
    if (removedIndex === null && addedIndex === null) return arr;

    const result = [...arr];
    // let itemToAdd = payload;

    // if (removedIndex !== null) {
    //   itemToAdd = result.splice(removedIndex, 1)[0];
    // }

    if (addedIndex !== null) {
      const length = data.length;
      let index = 0;

      if (!length) index = 0;
      else if (addedIndex === 0) index = data[0].index - 1;
      else if (addedIndex === length - 1) index = data[length - 1].index + 1;
      else
        index = (data[addedIndex - 1].index + data[addedIndex + 1].index) / 2;

      dispatch(
        updateTodoThunk({
          id: payload.id,
          newTodo: { status: column.status, index },
        })
      );
    }

    return result;
  };

  const getCardPayload = (columnId, index) => {
    return data[index];
  };

  const onCardDrop = (columnId, dropResult) => {
    if (dropResult.removedIndex !== null || dropResult.addedIndex !== null) {
      const newColumn = applyDrag(data, dropResult);
      setData([...newColumn]);
    }
  };

  return (
    <Box
      textTransform={'uppercase'}
      bg='#f1f3f5'
      flex={1}
      height='70vh'
      p='4px 8px'
      fontSize='14px'
      fontWeight='600'
    >
      {column.name}
      <Box>
        <Container
          orientation='vertical'
          className='card-container'
          groupName='col'
          onDrop={(e) => onCardDrop(column.id, e)}
          getChildPayload={(index) => getCardPayload(column.id, index)}
          dragClass='card-ghost'
          dropClass='card-ghost-drop'
          dropPlaceholder={{
            animationDuration: 150,
            showOnTop: true,
            className: 'drop-preview',
          }}
          dropPlaceholderAnimationDuration={200}
        >
          {data.map((data) => (
            <Draggable key={data.id}>
              <Todo key={data.id} data={data} />
            </Draggable>
          ))}
        </Container>
      </Box>
    </Box>
  );
};

export default TodoItems;

import { Flex } from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { todosRemaining } from '../../redux/selectors.js';
import { getAllTodosThunk } from '../../redux/todoSlice.js';
import { getAllUsersThunk } from '../../redux/userSlice.js';
import projectSlice from '../../redux/projectSlice.js';
import useNotification from '../../hook/useNotification.js';
import TodoItems from './TodoItems.js';
import { useParams } from 'react-router-dom';

const columns = [
  {
    name: 'backlog',
    id: 0,
    status: 'backlog',
  },
  {
    name: 'select for development',
    id: 1,
    status: 'select-for-development',
  },
  {
    name: 'in progress',
    id: 2,
    status: 'in-progress',
  },
  {
    name: 'done',
    id: 3,
    status: 'done',
  },
];

const TodoList = () => {
  const todos = useSelector(todosRemaining);
  const dispatch = useDispatch();
  const { sendNotification } = useNotification();
  const params = useParams();

  useEffect(() => {
    sendNotification(dispatch(getAllTodosThunk()));
    sendNotification(dispatch(getAllUsersThunk()));
    dispatch(projectSlice.actions.setProjectID(params.id));
  }, []);

  return (
    <Flex width='100%' gap={'16px'}>
      {columns.map((column) => (
        <TodoItems key={column.id} column={column} todos={todos} />
      ))}
    </Flex>
  );
};

export default TodoList;

import { Box, Button } from '@chakra-ui/react';
import {
  getAuth,
  getRedirectResult,
  GoogleAuthProvider,
  signInWithRedirect,
} from 'firebase/auth';
import { FcGoogle } from 'react-icons/fc';
import { app } from '../../firebase/firebaseConfig.js';
import useNotification from '../../hook/useNotification.js';

const LoginWithGoogle = () => {
  const provider = new GoogleAuthProvider();
  const auth = getAuth(app);
  const { sendNotification } = useNotification();

  const signInHandler = () => {
    signInWithRedirect(auth, provider);
  };

  (async () => {
    const result = await getRedirectResult(auth);

    if (result) {
      localStorage.setItem('token', result.user.accessToken);
      localStorage.setItem('email', result.user.email);
      localStorage.setItem('name', result.user.displayName);
      localStorage.setItem('avatar', result.user.photoURL);

      sendNotification(
        Promise.resolve({
          payload: { message: 'Login with google successfully!' },
        }),
        true
      );
    }
  })();

  return (
    <Box marginTop='8px'>
      <Button leftIcon={<FcGoogle />} width='100%' onClick={signInHandler}>
        Login with google
      </Button>
    </Box>
  );
};

export default LoginWithGoogle;
